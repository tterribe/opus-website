var searchData=
[
  ['opusfilecallbacks_26',['OpusFileCallbacks',['../structOpusFileCallbacks.html',1,'']]],
  ['opushead_27',['OpusHead',['../structOpusHead.html',1,'']]],
  ['opuspicturetag_28',['OpusPictureTag',['../structOpusPictureTag.html',1,'']]],
  ['opusserverinfo_29',['OpusServerInfo',['../structOpusServerInfo.html',1,'']]],
  ['opustags_30',['OpusTags',['../structOpusTags.html',1,'']]],
  ['output_5fgain_31',['output_gain',['../structOpusHead.html#a1f1c9e144ab05fe281f088d3e73eeab2',1,'OpusHead']]],
  ['opening_20and_20closing_32',['Opening and Closing',['../group__stream__open__close.html',1,'']]]
];
