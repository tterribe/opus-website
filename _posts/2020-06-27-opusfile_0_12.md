---
title: opusfile 0.12
date: 2020-06-27
categories: ["release", "dev"]
layout: post
component: opusfile
---

The opusfile library provides seeking, decode, and playback
of Opus streams in the Ogg container (.opus files) including
over http(s) on posix and windows systems.

opusfile depends on libopus and libogg.
The included opusurl library for http(s) access depends on
opusfile and openssl.

Changes since the v0.11 release:

 - Fix stack overflow buffering out-of-sequence streams.
 - Fix possible divide-by-zero.
 - Fix issues with seeking in the win32 backend.
 - Fix an issue where the seek algorithm could be confused
   by stream data changing between reads.
 - Clean up compiler and scan-build warnings.
 - Avoid use of the deprecated ftime() function
   which has Y2038 problems.
 - Remove undefined behaviour memcpy(NULL) in op_read_native().
 - Visual Studio project files updated for libogg 1.3.4 library name change.
 - Various build systems updates.
 - Various integration and testing environment improvements.

This release is backward-compatible with the previous
release. We recommend all users upgrade.

Note that because of the removal of certificate store hooks
in openssl 1.1.1 and later, there are unfortunately no
supported versions of that library which can be used with
the code in opusurl to validate https responses against
the system certificate store on Windows. Using the system
default access to the certificate store on other platforms
works fine.

Source code:
[opusfile-0.12.tar.gz](https://downloads.xiph.org/releases/opus/opusfile-0.12.tar.gz),
[opusfile-0.12.zip](https://downloads.xiph.org/releases/opus/opusfile-0.12.zip).

SHA-256 checksums:

    118d8601c12dd6a44f52423e68ca9083cc9f2bfe72da7a8c1acb22a80ae3550b  opusfile-0.12.tar.gz
    7f44575596b78d7787c1865b9653e2a71546ff1ae77d87c53ab16dcc7af295ba  opusfile-0.12.zip

No Windows build is available for this release.
Developers should integrate the source code directly into their applications.

Programming documentation is available in tree and
[online](https://opus-codec.org/docs/).

The library is functional, but there are likely issues
we didn't find in our own testing. Please give feedback
in #opus on irc.libera.chat, opus@xiph.org, or at
[gitlab](https://gitlab.xiph.org/xiph/opusfile).
